import { Injectable } from '@angular/core';
import * as firebase from 'firebase/compat/app';
import { AngularFirestore } from "@angular/fire/compat/firestore";
import { AngularFireDatabase } from '@angular/fire/compat/database';
import { AngularFireAuth } from "@angular/fire/compat/auth";
import { AngularFireStorage } from '@angular/fire/compat/storage';

export interface User {
  uid: string;
  email: string;
}
 
export interface Message {
  createdAt: any;
  id: string;
  from: string;
  msg: string;
  fromName: string;
  myMsg: boolean;
}
 
@Injectable({
  providedIn: 'root'
})

export class ChatServiceService {
  currentUser: User = null;
  collection:any = 'messages';
  constructor(private firestore: AngularFirestore, private auth: AngularFireAuth, private db: AngularFireDatabase) { 
  }
 
  signIn({ email, password }) {
    return this.auth.signInWithEmailAndPassword(email, password);
  }
  
  async signup({ email, password }): Promise<any> {
    const credential = await this.auth.createUserWithEmailAndPassword(
      email,
      password
    );
 
    const uid = credential.user.uid;
 
    return this.firestore.doc(
      `users/${uid}`
    ).set({
      uid,
      email: credential.user.email,
    })
  }

  signOut(){
    return this.auth.signOut();
  }

  create(msg) {
    return this.firestore.collection('messages').add({
      msg: msg,
      from: this.currentUser.uid,
      // createdAt: firebase.firestore.
    });
  }

  read() {
    return this.firestore.collection(this.collection).snapshotChanges();
  }

  update(recordID, record) {
    this.firestore.doc(this.collection + '/' + recordID).update(record);
  }

  delete(record_id) {
    this.firestore.doc(this.collection + '/' + record_id).delete();
  }
}
