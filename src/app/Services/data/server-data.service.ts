import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController, MenuController, NavController, ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class ServerDataService {

  menu : any = [
    {
      "id": 1,
      "name" : "Midnight Deals",
      "menu" : [
        {
            "id": 1,
            "name" : "MN Deal 1",
            "detail" : "7-inch P.pan Pizza",
            "price" : "350",
            "image" : "assets/item.png",
            "selections" : [
                {
                    "id" : 1,
                    "name" : "Choose Your Crust",
                    "required" : true,
                    "options" : [
                        {
                            "id" : 1,
                            "name" : "Deep Pan (Signature)"
                        },
                        {
                            "id" : 2,
                            "name" : "Hand Tossed"
                        },
                        {
                            "id" : 3,
                            "name" : "Thin style"
                        }
                    ]
                },
                {
                    "id" : 2,
                    "name" : "Pizza Flavors",
                    "required": true,
                    "options" : [
                        {
                            "id" : 1,
                            "name" : "Garlic BBQ Tikka"
                        },
                        {
                            "id" : 2,
                            "name" : "Creamy Super Max"
                        },
                        {
                            "id" : 3,
                            "name" : "Peri Max"
                        },
                        {
                            "id" : 4,
                            "name" : "Chicken Max"
                        },
                        {
                            "id" : 5,
                            "name" : "Creamy Tikka"
                        }
                    ]
                }
            ]
        },
        {
            "id": 1,
            "name" : "MN Deal 1",
            "detail" : "7-inch P.pan Pizza",
            "price" : "350",
            "image" : "assets/item.png",
            "selections" : [
                {
                    "id" : 1,
                    "name" : "Choose Your Crust",
                    "required" : true,
                    "options" : [
                        {
                            "id" : 1,
                            "name" : "Deep Pan (Signature)"
                        },
                        {
                            "id" : 2,
                            "name" : "Hand Tossed"
                        },
                        {
                            "id" : 3,
                            "name" : "Thin style"
                        }
                    ]
                },
                {
                    "id" : 2,
                    "name" : "Pizza Flavors",
                    "required": true,
                    "options" : [
                        {
                            "id" : 1,
                            "name" : "Garlic BBQ Tikka"
                        },
                        {
                            "id" : 2,
                            "name" : "Creamy Super Max"
                        },
                        {
                            "id" : 3,
                            "name" : "Peri Max"
                        },
                        {
                            "id" : 4,
                            "name" : "Chicken Max"
                        },
                        {
                            "id" : 5,
                            "name" : "Creamy Tikka"
                        }
                    ]
                }
            ]
        },
      ]
    },
    {
      "id": 2,
      "name" : "App Promo Deals",
      "menu" : [
          {
              "id": 1,
              "name" : "MN Deal 1",
              "detail" : "7-inch P.pan Pizza",
              "price" : "350",
              "image" : "assets/item.png",
              "selections" : [
                  {
                      "id" : 1,
                      "name" : "Choose Your Crust",
                      "required" : true,
                      "options" : [
                          {
                              "id" : 1,
                              "name" : "Deep Pan (Signature)"
                          },
                          {
                              "id" : 2,
                              "name" : "Hand Tossed"
                          },
                          {
                              "id" : 3,
                              "name" : "Thin style"
                          }
                      ]
                  },
                  {
                      "id" : 2,
                      "name" : "Pizza Flavors",
                      "required": true,
                      "options" : [
                          {
                              "id" : 1,
                              "name" : "Garlic BBQ Tikka"
                          },
                          {
                              "id" : 2,
                              "name" : "Creamy Super Max"
                          },
                          {
                              "id" : 3,
                              "name" : "Peri Max"
                          },
                          {
                              "id" : 4,
                              "name" : "Chicken Max"
                          },
                          {
                              "id" : 5,
                              "name" : "Creamy Tikka"
                          }
                      ]
                  }
              ]
          }
      ]
    },
    {
      "id": 3,
      "name" : "Max Value Deals",
      "menu" : [
          {
              "id": 1,
              "name" : "MN Deal 1",
              "detail" : "7-inch P.pan Pizza",
              "price" : "350",
              "image" : "assets/item.png",
              "selections" : [
                  {
                      "id" : 1,
                      "name" : "Choose Your Crust",
                      "required" : true,
                      "options" : [
                          {
                              "id" : 1,
                              "name" : "Deep Pan (Signature)"
                          },
                          {
                              "id" : 2,
                              "name" : "Hand Tossed"
                          },
                          {
                              "id" : 3,
                              "name" : "Thin style"
                          }
                      ]
                  },
                  {
                      "id" : 2,
                      "name" : "Pizza Flavors",
                      "required": true,
                      "options" : [
                          {
                              "id" : 1,
                              "name" : "Garlic BBQ Tikka"
                          },
                          {
                              "id" : 2,
                              "name" : "Creamy Super Max"
                          },
                          {
                              "id" : 3,
                              "name" : "Peri Max"
                          },
                          {
                              "id" : 4,
                              "name" : "Chicken Max"
                          },
                          {
                              "id" : 5,
                              "name" : "Creamy Tikka"
                          }
                      ]
                  }
              ]
          }
      ]
    },
    {
      "id": 4,
      "name" : "2 Big 2 Better",
      "menu" : [
          {
              "id": 1,
              "name" : "MN Deal 1",
              "detail" : "7-inch P.pan Pizza",
              "price" : "350",
              "image" : "assets/item.png",
              "selections" : [
                  {
                      "id" : 1,
                      "name" : "Choose Your Crust",
                      "required" : true,
                      "options" : [
                          {
                              "id" : 1,
                              "name" : "Deep Pan (Signature)"
                          },
                          {
                              "id" : 2,
                              "name" : "Hand Tossed"
                          },
                          {
                              "id" : 3,
                              "name" : "Thin style"
                          }
                      ]
                  },
                  {
                      "id" : 2,
                      "name" : "Pizza Flavors",
                      "required": true,
                      "options" : [
                          {
                              "id" : 1,
                              "name" : "Garlic BBQ Tikka"
                          },
                          {
                              "id" : 2,
                              "name" : "Creamy Super Max"
                          },
                          {
                              "id" : 3,
                              "name" : "Peri Max"
                          },
                          {
                              "id" : 4,
                              "name" : "Chicken Max"
                          },
                          {
                              "id" : 5,
                              "name" : "Creamy Tikka"
                          }
                      ]
                  }
              ]
          }
      ]
    }
  ]
  
  constructor(private nav: NavController, private alert: AlertController, private t: ToastController, private router: Router,
    private m : MenuController
    ) { }

  async toast(msg, duration=3000){
    let a = await this.t.create({
      message: msg,
      duration: duration,
      mode: 'ios'
    });

    a.present();
  }

  root(page = 'home') {
    this.nav.navigateRoot(page);
  }

  next(page, data = {}) {
    this.nav.navigateForward(page, { state: data });
  }

  getParams() {
    return this.router.getCurrentNavigation().extras.state;
  }

  back() {
    this.nav.back();
  }

  async menuToggle(id) {
    this.m.enable(true, id);
    this.m.toggle();
  }

  setData(data= {}){
    let a = new FormData();

    for(let [k,v] of Object.entries(data)){
      a.append(k, v.toString());
    }

    return a;
  }
}
