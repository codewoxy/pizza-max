import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ServerDataService } from 'src/app/Services/data/server-data.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {

  @ViewChild('password', {read: ElementRef}) password: ElementRef;
  @ViewChild('cpassword', {read: ElementRef}) cpassword: ElementRef;
  @ViewChild('passIcon', {read: ElementRef}) passIcon: ElementRef;
  @ViewChild('cpassIcon', {read: ElementRef}) cpassIcon: ElementRef;
  constructor(public sd: ServerDataService) { }

  ngOnInit() {
  }

  showPassword(){
    let pass = this.password.nativeElement;
    let cpass = this.cpassword.nativeElement;
    let icon = this.passIcon.nativeElement;
    let cicon = this.cpassIcon.nativeElement;
    
    if(pass.type == 'password'){
      pass.type = 'text';
      cpass.type = 'text';
      icon.name = 'eye-off-outline'
      cicon.name = 'eye-off-outline'
    }else{
      pass.type = 'password';
      cpass.type = 'password';
      icon.name = 'eye-outline'
      cicon.name = 'eye-outline'
    }
  }
}
