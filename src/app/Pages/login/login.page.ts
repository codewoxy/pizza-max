import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Platform } from '@ionic/angular';
import { ChatServiceService } from 'src/app/Services/chat/chat-service.service';
import { ServerDataService } from 'src/app/Services/data/server-data.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  @ViewChild('password', {read: ElementRef}) password: ElementRef;
  @ViewChild('passIcon', {read: ElementRef}) passIcon: ElementRef;
  constructor(public sd: ServerDataService, public chat: ChatServiceService, public platform: Platform) { }

  ngOnInit() {
  }

  showPassword(){
    let pass = this.password.nativeElement;
    let passIcon = this.passIcon.nativeElement;

    if(pass.type == 'password'){
      pass.type = 'text';
      passIcon.name = 'eye-off-outline';
    }else{
      pass.type = 'password';
      passIcon.name = 'eye-outline';
    }
  }

  // doLogin(){
  //   let params: any;
  //   if (this.platform.is('cordova')) {
  //     if (this.platform.is('android')) {
  //       params = {
  //         webClientId: '<WEB_CLIENT_ID>', //  webclientID 'string'
  //         offline: true
  //       };
  //     } else {
  //       params = {};
  //     }
  //     this.google.login(params)
  //     .then((response) => {
  //       const { idToken, accessToken } = response;
  //       this.onLoginSuccess(idToken, accessToken);
  //     }).catch((error) => {
  //       console.log(error);
  //       alert('error:' + JSON.stringify(error));
  //     });
  //   } else{
  //     console.log('else...');
  //     this.fireAuth.signInWithPopup(new firebase.auth.GoogleAuthProvider()).then(success => {
  //       console.log('success in google login', success);
  //       this.isGoogleLogin = true;
  //       this.user =  success.user;
  //     }).catch(err => {
  //       console.log(err.message, 'error in google login');
  //     });
  //   }
  // }

}
