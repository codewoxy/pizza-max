import { Component, OnInit } from '@angular/core';
import { ServerDataService } from 'src/app/Services/data/server-data.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.page.html',
  styleUrls: ['./search.page.scss'],
})
export class SearchPage implements OnInit {

  list:any = [
    {
      "id": 1,
      "name" : "MN Deal 1",
      "detail" : "7-inch P.pan Pizza",
      "price" : "350",
      "image" : "assets/item.png",
      "selections" : [
          {
              "id" : 1,
              "name" : "Choose Your Crust",
              "required" : true,
              "options" : [
                  {
                      "id" : 1,
                      "name" : "Deep Pan (Signature)"
                  },
                  {
                      "id" : 2,
                      "name" : "Hand Tossed"
                  },
                  {
                      "id" : 3,
                      "name" : "Thin style"
                  }
              ]
          },
          {
              "id" : 2,
              "name" : "Pizza Flavors",
              "required": true,
              "options" : [
                  {
                      "id" : 1,
                      "name" : "Garlic BBQ Tikka"
                  },
                  {
                      "id" : 2,
                      "name" : "Creamy Super Max"
                  },
                  {
                      "id" : 3,
                      "name" : "Peri Max"
                  },
                  {
                      "id" : 4,
                      "name" : "Chicken Max"
                  },
                  {
                      "id" : 5,
                      "name" : "Creamy Tikka"
                  }
              ]
          }
      ]
    }
  ]
  constructor(public sd: ServerDataService) { }

  ngOnInit() {
  }

  search(ev){
    console.log(ev.detail.value)
  }

  selectChange(e) {
    console.log(e);
  }

}
