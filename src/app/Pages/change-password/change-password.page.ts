import { Component, OnInit } from '@angular/core';
import { ServerDataService } from 'src/app/Services/data/server-data.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.page.html',
  styleUrls: ['./change-password.page.scss'],
})
export class ChangePasswordPage implements OnInit {

  constructor(public sd: ServerDataService) { }

  ngOnInit() {
  }

}
