import { Component, ElementRef, OnInit, Renderer2, ViewChild } from '@angular/core';
import { IonContent } from '@ionic/angular';
import { ServerDataService } from 'src/app/Services/data/server-data.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.page.html',
  styleUrls: ['./menu.page.scss'],
})
export class MenuPage implements OnInit {
  slideOpts = {
    initialSlide: 0,
    speed: 400,
    autoplay: true,
    loop: true
  };

  stickyTop:any=0;
  @ViewChild('categoryList', {read: ElementRef}) categoryList: ElementRef;
  @ViewChild('menuList', {read: ElementRef}) menuList: ElementRef;
  @ViewChild(IonContent) content: IonContent;

  isCart = true;
  constructor(public sd: ServerDataService, public renderer: Renderer2) { }

  ngOnInit() {
  }

  ionViewDidEnter(){
    window.setTimeout(()=>{
      let el = this.categoryList.nativeElement
      console.log(el.getBoundingClientRect())
      this.stickyTop = el.getBoundingClientRect().top;
    }, 400)
  }

  onScroll(ev){
    const offset = ev.detail.scrollTop; 
    let a = this.categoryList.nativeElement.getBoundingClientRect().top;
    if(offset > 2*(a)){
      this.renderer.addClass(this.categoryList.nativeElement, 'sticky')
    }else{
      this.renderer.removeClass(this.categoryList.nativeElement, 'sticky')
    }
  }

  selectCategory(i) {
    let arr = this.menuList.nativeElement.children;  
    let item = arr[i];
    item.scrollIntoView({behavior : 'smooth', block: 'center'})
  }

  toggleCart(){
    this.isCart = !this.isCart
  }
}
