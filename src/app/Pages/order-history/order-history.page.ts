import { Component, OnInit } from '@angular/core';
import { ServerDataService } from 'src/app/Services/data/server-data.service';

@Component({
  selector: 'app-order-history',
  templateUrl: './order-history.page.html',
  styleUrls: ['./order-history.page.scss'],
})
export class OrderHistoryPage implements OnInit {

  constructor(public sd: ServerDataService) { }

  ngOnInit() {
  }

}
