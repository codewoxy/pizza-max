import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ServerDataService } from 'src/app/Services/data/server-data.service';

@Component({
  selector: 'app-order-print',
  templateUrl: './order-print.page.html',
  styleUrls: ['./order-print.page.scss'],
})
export class OrderPrintPage implements OnInit {

  @ViewChild('printSection', {read: ElementRef}) section: ElementRef;

  constructor(public sd: ServerDataService) { }

  ngOnInit() {
  }

}
