import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OrderPrintPage } from './order-print.page';

const routes: Routes = [
  {
    path: '',
    component: OrderPrintPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OrderPrintPageRoutingModule {}
