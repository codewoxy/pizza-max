import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OrderPrintPageRoutingModule } from './order-print-routing.module';

import { OrderPrintPage } from './order-print.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OrderPrintPageRoutingModule
  ],
  declarations: [OrderPrintPage]
})
export class OrderPrintPageModule {}
