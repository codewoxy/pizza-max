import { Component, OnInit } from '@angular/core';
import { ServerDataService } from 'src/app/Services/data/server-data.service';

@Component({
  selector: 'app-address-add',
  templateUrl: './address-add.page.html',
  styleUrls: ['./address-add.page.scss'],
})
export class AddressAddPage implements OnInit {

  constructor(public sd: ServerDataService) { }

  ngOnInit() {
  }

}
