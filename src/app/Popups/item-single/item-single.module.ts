import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ItemSinglePageRoutingModule } from './item-single-routing.module';

import { ItemSinglePage } from './item-single.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ItemSinglePageRoutingModule
  ],
  declarations: [ItemSinglePage]
})
export class ItemSinglePageModule {}
