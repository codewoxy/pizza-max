import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ItemSinglePage } from './item-single.page';

const routes: Routes = [
  {
    path: '',
    component: ItemSinglePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ItemSinglePageRoutingModule {}
