import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: () => import('./Pages/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'signup',
    loadChildren: () => import('./Pages/signup/signup.module').then( m => m.SignupPageModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./Pages/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'item-single',
    loadChildren: () => import('./Popups/item-single/item-single.module').then( m => m.ItemSinglePageModule)
  },
  {
    path: 'address-list',
    loadChildren: () => import('./Pages/address-list/address-list.module').then( m => m.AddressListPageModule)
  },
  {
    path: 'address-add',
    loadChildren: () => import('./Pages/address-add/address-add.module').then( m => m.AddressAddPageModule)
  },
  {
    path: 'address-update',
    loadChildren: () => import('./Pages/address-update/address-update.module').then( m => m.AddressUpdatePageModule)
  },
  {
    path: 'payment-card',
    loadChildren: () => import('./Pages/payment-card/payment-card.module').then( m => m.PaymentCardPageModule)
  },
  {
    path: 'cart',
    loadChildren: () => import('./Pages/cart/cart.module').then( m => m.CartPageModule)
  },
  {
    path: 'checkout',
    loadChildren: () => import('./Pages/checkout/checkout.module').then( m => m.CheckoutPageModule)
  },
  {
    path: 'profile',
    loadChildren: () => import('./Pages/profile/profile.module').then( m => m.ProfilePageModule)
  },
  {
    path: 'order-history',
    loadChildren: () => import('./Pages/order-history/order-history.module').then( m => m.OrderHistoryPageModule)
  },
  {
    path: 'change-password',
    loadChildren: () => import('./Pages/change-password/change-password.module').then( m => m.ChangePasswordPageModule)
  },
  {
    path: 'contact-us',
    loadChildren: () => import('./Pages/contact-us/contact-us.module').then( m => m.ContactUsPageModule)
  },
  {
    path: 'favourite',
    loadChildren: () => import('./Pages/favourite/favourite.module').then( m => m.FavouritePageModule)
  },
  {
    path: 'search',
    loadChildren: () => import('./Pages/search/search.module').then( m => m.SearchPageModule)
  },
  {
    path: 'select-location',
    loadChildren: () => import('./Popups/select-location/select-location.module').then( m => m.SelectLocationPageModule)
  },
  {
    path: 'menu',
    loadChildren: () => import('./Pages/menu/menu.module').then( m => m.MenuPageModule)
  },
  {
    path: 'forgot-password',
    loadChildren: () => import('./Pages/forgot-password/forgot-password.module').then( m => m.ForgotPasswordPageModule)
  },
  {
    path: 'order-print',
    loadChildren: () => import('./Pages/order-print/order-print.module').then( m => m.OrderPrintPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
